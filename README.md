<div id="top"></div>

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- [![Appveyor Build Status][appveyor-build-shield]][appveyor-build-url] -->

[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]

<!-- [![Issues][issues-shield]][issues-url] -->

[![LICENSE][license-shield]][license-url]

<br />
<div align="center">
  <a href="https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae">
    <img src="./extras/project-logo/logo.jpg" alt="Logo" width="500">
  </a>

<h3 align="center">Acoustic Behavior of Panamanian Arctiinae</h3>

  <p align="center">
    Study examining the acoustic responses of some tiger moths to bat echolocation calls in Panama
    <br />
    <a href="https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/issues">Request Feature</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
            <ul>
                <li><a href="#git">Git</a></li>
                <li><a href="#make">Make</a></li>
            </ul>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#setup">Setup</a></li>
      </ul>
    </li>
    <!--<li><a href="#usage">Usage</a></li>-->
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

<div id="about-the-project"></div>

## About The Project

We are studying the acoustic responses of some tiger moths to bat echolocation calls in Panama.

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->

<!--[![video_thumbnail](LINK-TO-IMAGE)](PROJECT_VIDEO_URL)-->

<p align="right">(<a href="#top">back to top</a>)</p>

<div id="built-with"></div>

<!-- ### Funding

Generous funding was provided by:

- NA <a href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=NA">NA</a> -->

### How To Cite The Project

See the [CITATION](https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/blob/master/CITATION.md) document.

### Built With

- Love

<p align="right">(<a href="#top">back to top</a>)</p>

<div id="getting-started"></div>

## Getting Started

To get up and running follow these simple steps.

<div id="roadmap"></div>

## Roadmap

- [ ] Extract acoustic parameters from audio files

See the [open issues](https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>

<div id="contributing"></div>

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

See [CONTRIBUTING](https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/blob/master/CONTRIBUTING.md) for more information about contributing!

<p align="right">(<a href="#top">back to top</a>)</p>

<div id="license"></div>

## License

Distributed under the GNU General Public License. See [LICENSE](https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/blob/master/LICENSE) for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

<div id="contact"></div>

## Contact

<!-- Note: gitlab will remove inline styles for security -->
<div>
    <img style="
        width: 150px;
        max-height: 150px;
        border-radius: 15%;
        background-position: center;
        background-repeat: no-repeat;
        float: left;    
        margin: 0 15px 0 0;" src="./extras/author-images/njdowdy.jpg">
    <div>
        <div style="font-size: 20px;">Dr. Nicolas J. Dowdy</div>
        <div style="font-size: 15px;">Director of Collections Informatics & Head of Zoology</div>
        <div style="font-size: 15px;">Milwaukee Public Museum, Department of Zoology</div>
    </div>
</div>

[![ORCID](https://img.shields.io/badge/ORCID-ID-brightgreen)](https://orcid.org/0000-0002-5453-2569)
[![TWITTER](https://img.shields.io/twitter/follow/njdowdy1?style=social)](http://www.twitter.com/njdowdy1)

---

For a complete list of authors and major contributors, see [AUTHORS](https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/blob/master/AUTHORS.md)

Project Link: [https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae](https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae)  
Project Template Link: [https://gitlab.com/njdowdy/manuscript-template](https://gitlab.com/njdowdy/manuscript-template)

<p align="right">(<a href="#top">back to top</a>)</p>

<div id="acknowledgments"></div>

## Acknowledgments

- Starter code: [best-README-template](https://gitlab.com/othneildrew/Best-README-Template)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[appveyor-build-shield]: https://img.shields.io/appveyor/build/njdowdy/acoustic-behavior-of-panamanian-arctiinae?style=for-the-badge
[appveyor-build-url]: https://ci.appveyor.com/project/njdowdy/acoustic-behavior-of-panamanian-arctiinae
[contributors-shield]: https://img.shields.io/gitlab/contributors/njdowdy/acoustic-behavior-of-panamanian-arctiinae.svg?style=for-the-badge
[contributors-url]: https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/graphs/contributors
[forks-shield]: https://img.shields.io/gitlab/forks/njdowdy/acoustic-behavior-of-panamanian-arctiinae.svg?style=for-the-badge&label=Fork
[forks-url]: https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/network/members
[stars-shield]: https://img.shields.io/gitlab/stars/njdowdy/acoustic-behavior-of-panamanian-arctiinae.svg?style=for-the-badge&label=Star
[stars-url]: https://img.shields.io/gitlab/stars/njdowdy/acoustic-behavior-of-panamanian-arctiinae/stargazers
[issues-shield]: https://img.shields.io/gitlab/issues/njdowdy/acoustic-behavior-of-panamanian-arctiinae.svg?style=for-the-badge
[issues-url]: https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/issues
[license-shield]: https://img.shields.io/gitlab/license/njdowdy/acoustic-behavior-of-panamanian-arctiinae.svg?style=for-the-badge
[license-url]: https://gitlab.com/njdowdy/acoustic-behavior-of-panamanian-arctiinae/blob/master/LICENSE
[product-screenshot]: ./extras/project-logo/screenshot.jpg
